variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_path" {}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "us-east-1"
}

variable "test1" {
  type = "string"

  #  default = "fie-green-2017689696.us-east-1.elb.amazonaws.com"
}

variable "test2" {
  type = "string"

  #  default = "fie-blue-694393339.us-east-1.elb.amazonaws.com"
}

variable "alb_zone_id" {
  default = "Z35SXDOTRQ7X7K"
}
