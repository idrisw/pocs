#route53 zone - fandiexpress.com.
data "aws_route53_zone" "fandiexpress" {
  name = "fandiexpress.com."
}

#route53 records - autofinancealliance

#resource "aws_route53_record" "test1" {
#  zone_id = "${data.aws_route53_zone.fandiexpress.zone_id}"
#  name    = "${data.aws_route53_zone.fandiexpress.name}"
#  type    = "A"

#  alias {
#    name                   = "${var.test1}"
#    zone_id                = "${var.alb_zone_id}"
#    evaluate_target_health = false
#  }
#}

resource "aws_route53_record" "test1" {
  zone_id = "${data.aws_route53_zone.fandiexpress.zone_id}"
  name    = "test1.${data.aws_route53_zone.fandiexpress.name}"
  type    = "A"

  alias {
    name                   = "${var.test1}"
    zone_id                = "${var.alb_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "test2" {
  zone_id = "${data.aws_route53_zone.fandiexpress.zone_id}"
  name    = "test2.${data.aws_route53_zone.fandiexpress.name}"
  type    = "A"

  alias {
    name                   = "${var.test2}"
    zone_id                = "${var.alb_zone_id}"
    evaluate_target_health = false
  }
}
